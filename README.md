# Resume FVT-govdrop-analysis
This repository contains the dataset and the report for the analysis of finance.vote's first governance drop. 
The analysis was made by @[Masser#6568](https://twitter.com/CryptoMasser).
The overall research questions were: Was the airdrop fair? How can we improve the next governance drop.

## Files
-  [dataset](https://docs.google.com/spreadsheets/d/1S9anof0nzEc8nApYKmOl9HTM_guCPlJONRIhKi_hAsk/edit#gid=1058649493) 
-  [report](https://gitlab.com/the-datadao/fvt-govdrop-analysis/-/blob/main/Governance_Drop_Report_-_Masser.pdf)

## Author 
@[Masser#6568](https://twitter.com/CryptoMasser)

### Reviewer
@[Lizzl#8507](https://twitter.com/_datadeo_)

## License
The dataset and the infopgraphic is open for everyone to use. 
If you make amendments to the infographic, please get in touch with the author first.

## Project status
Completed 
